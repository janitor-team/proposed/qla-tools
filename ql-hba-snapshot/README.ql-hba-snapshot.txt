﻿
                       FC HBA Snapshot Utility
                                Readme

                         QLogic Corporation.
                         All rights reserved.  


Table of Contents

1. Package Contents 
2. Requirements  
3. OS Support 
4. Supported Features 
5. Using the FC HBA Snapshot Utility  
   5.1 Command Line Options  
   5.2 Usage Examples  
6. Application Notes 
7. Known Issues and Workarounds 
8. Contacting Support  


1. Package Contents 

The FC HBA Snapshot Utility package contains the following: 

 * COPYING - GNU General Public License that describes user rights to
   copy, distribute, and use the open source content in this Linux 
   tool.

 * ql-hba-snapshot.sh - Script file used to display the details of the
   QLogic adapter attached to the system.

 * README.ql-hba-snapshot.txt - This readme file.

 * revision.qlhbasnapshot.txt - Text file that identifies the changes
   made between versions of this package. 


2. Requirements

The FC HBA Snapshot Utility requires one of the Linux platforms listed
in section 3, OS Support. 


3. OS Support

The FC HBA Snapshot Utility runs on the following OS platforms:

 * Red Hat RHEL AS 4.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64

 * Red Hat RHEL AS 5.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64

 * Novell SLES 9 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64 
 
 * Novell SLES 10 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64
 
NOTE: For specific OS service packs (SP) and updates, refer to the 
descriptions where this software version is posted on the QLogic Web
site:
 http://support.qlogic.com/support/drivers_software.aspx


4. Supported Features

The FC HBA Snapshot Utility provides the following features:

 * Displays specific information of the adapter, such as world wide
   port number (WWPN), port ID, and connected targets.
 
 * Lists QLogic adapters in the system.
 
 * Provides detailed adapter information, including LUNs.
 
 * Provides parameters or options that can be passed to the QLogic 
   driver.
 
 * Displays statistics for the QLogic host. This option is not 
   available for the standard driver.

 * Supports Fibre Channel driver version 8.xx.xx.


5. Using the FC HBA Snapshot Utility 

This utility displays the details of the QLogic adapter attached to 
the system. It uses one of the two file systems for scanning: proc 
or sysfs.

Displayed details include the following:

 * QLogic adapter general information
 * Targets connected to the QLogic host
 * Details about targets and LUNs
 * Statistics and parameters
 * Host list

By default, the utility displays specific information for the QLogic
adapter, as described in the following sections:
 
 * 5.1 Command Line Options  
 * 5.2 Usage Examples 

5.1 Command Line Options

<Host number> | <-a/--all> [optional]

     Provides the <HOST NUMBER> of the adapter to display its detailed
     information. If you do not specify host number, this command 
     shows general information for all hosts. For example:

     # ./ql-hba-snapshot.sh <Host Number>

-h, --help 

     Displays the help text. For example:

      # ./ql-hba-snapshot.sh --help

-hl, --hostlist

     Displays the list of QLogic hosts (adapters) connected to this 
     system. To obtain the <HOST NUMBER>, enter the following command:

      # ./ql-hba-snapshot.sh --hostlist

-p, --parameters

     Displays the command line parameters that can be passed to the 
     QLogic adapter driver. For example:

      # ./ql-hba-snapshot.sh --parameters <Host Number>
      # ./ql-hba-snapshot.sh --procfs --parameters (for procfs)

-s, --statistics

     Displays statistical information for the specified host. The 
     statistics option is only supported in a sysfs-based scan. For 
     example:

      # ./ql-hba-snapshot.sh --statistics <Host number/--all>

-proc, --procfs

     Forces the utility to scan procfs-base information, instead of 
     the default sysfs. For example:

      # ./ql-hba-snapshot.sh --procfs [other valid option]

-v, --verbose

     Enables verbose display. Using this option shows detailed LUN 
     information, in addition to standard information. For example:

      # ./ql-hba-snapshot.sh --verbose <Host number/-a>

5.2 Usage Examples

The following examples show how to apply the command line options in
PROC-based and SYSFS-based file systems.

5.2.1 SYSFS-based File Systems

 * To display default adapter information, enter the following 
   command:

    # ./ql-hba-snapshot.sh 
		
 * To display detailed information of host 7, enter the following 
   command:

    # ./ql-hba-snapshot.sh 7

 * To display detailed information of all hosts, enter the following
   command:

    # ./ql-hba-snapshot.sh --all  

 * To display QLogic adapters driver parameters, enter one of the 
   following commands:

    # ./ql-hba-snapshot.sh -p
    # ./ql-hba-snapshot.sh --parameters

 * To display the QLogic adapter, enter one of the following commands:

    # ./ql-hba-snapshot.sh -hl
    # ./ql-hba-snapshot.sh --hostlist

 * To view help, enter one of the following commands:

    # ./ql-hba-snapshot.sh -h
    # ./ql-hba-snapshot.sh --help

5.2.2 PROC-based File Systems

 * To display default adapter information, enter one of the following
   commands:

    # ./ql-hba-snapshot.sh -proc
    # ./ql-hba-snapshot.sh --procfs
		
 * To display detailed information of host 7, enter one of the 
   following commands:

    # ./ql-hba-snapshot.sh -proc 7
    # ./ql-hba-snapshot.sh --procfs 7

 * To display detailed information of all hosts, enter one of the 
   following commands:

    # ./ql-hba-snapshot.sh -proc -a  
    # ./ql-hba-snapshot.sh --procfs --all  

    NOTE: The preceding two options both support the -v/--verbose 
          parameter. For example:

          # ./ql-hba-snapshot.sh -proc -v 7
          # ./ql-hba-snapshot.sh --procfs --verbose --all  

 * To display QLogic adapter driver command line parameters, enter 
   one of the following commands:

    # ./ql-hba-snapshot.sh -proc -p
    # ./ql-hba-snapshot.sh --procfs --parameters

 * To display statistics of host 8, enter one of the following 
   commands:

    # ./ql-hba-snapshot.sh -proc -s 8
    # ./ql-hba-snapshot.sh --procfs --statistics 8

 * To display QLogic adapters, enter one of the following commands:

    # ./ql-hba-snapshot.sh -proc -hl
    # ./ql-hba-snapshot.sh --procfs --hostlist

 * To view help, enter one of the following commands:

    # ./ql-hba-snapshot.sh -h
    # ./ql-hba-snapshot.sh --help


6. Application Notes 

None


7. Known Issues and Workarounds 

None


8. Contacting Support 

Please feel free to contact your QLogic approved reseller or QLogic 
Technical Support at any phase of integration for assistance. QLogic
Technical Support can be reached by the following methods: 

 Web:    http://support.qlogic.com
 E-mail: support@qlogic.com


(c) Copyright 2009. All rights reserved worldwide. QLogic, the QLogic 
logo, and the Powered by QLogic logo are registered trademarks of
QLogic Corporation. All other brand and product names are trademarks 
or registered trademarks of their respective owners. 
 