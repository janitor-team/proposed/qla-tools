﻿
            Set Device Command Timeout Utility for Linux
                                Readme
 
                         QLogic Corporation.
                         All rights reserved. 


Table of Contents

1. Package Contents 
2. Requirements  
3. OS Support 
4. Supported Features 
5. Using the Set Device Command Timeout Utility 
   5.1 Starting the Utility
   5.2 Command Line Options 
   5.3 Menu Options
6. Additional Notes 
7. Known Issues and Workarounds 
8. Contacting Support  


1. Package Contents 

The Set Device Command Timeout Utility package contains the following: 

 * COPYING - GNU General Public License that describes user rights to
   copy, distribute, and use the open source content in this Linux 
   tool.

 * ql-set-cmd-timeout.sh - Script file used to set the timeout on the 
   devices connected to the QLogic FC adapter.

 * README.ql-set-cmd-timeout.txt - This readme file.

 * revision.qlsetcmdtimeout.txt - Text file that identifies the 
   changes made between versions of this package. 


2. Requirements

The Set Device Command Timeout Utility (ql-set-cmd-timeout) requires 
one of the Linux platforms listed in section 3, OS Support. 


3. OS Support

The ql-set-cmd-timeout utility runs on the following OS platforms:

 * Red Hat RHEL AS 4.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64

 * Red Hat RHEL AS 5.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64

 * Novell SLES 9 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64 
 
 * Novell SLES 10  (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64 

NOTE: For specific OS service packs (SP) and updates, refer to the
descriptions where this software version is posted on the QLogic Web
site:
 http://support.qlogic.com/support/drivers_software.aspx


4. Supported Features

The ql-set-cmd-timeout utility:

 * Allows you to set the timeout on devices by specifying the host 
   and target number using the command line.
  
 * Allows you to select a host to set timeout on targets connected to 
   the host using a menu.

 * Sets a common value for all hosts at the same time. 

 * Supports Fibre Channel driver version 8.xx.xx.


5. Using the Set Device Command Timeout Utility 

This utility allows you to set the timeout on the devices connected 
to the QLogic FC adapter. 
 
This timeout value applies to the commands sent to the device. This 
can help when target devices take longer to execute a command, for 
example under heavy I/O. Setting a longer timeout reduces the chance 
of the Linux SCSI mid-layer driver aborting the tasks after a 
timeout. 

The following sections describe how to use this utility:

 * 5.1 Starting the Utility
 * 5.2 Command Line Options
 * 5.3 Menu Options

5.1 Starting the Utility

To start this utility, enter the following command:
 
 # ./ql-set-cmd-timeout.sh HOST TARGET TIMEOUT
 
  where:

  HOST is the QLogic adapter number.
  TARGET is the target connected to the adapter.
  TIMEOUT is the new timeout value to be set on the devices under
  TARGET.

 For example:
	
 For /sys/bus/pci/drivers/qla2300/0000:05:01.0/host2/target2:0:0/
 HOST and TARGET will be
 HOST=2  (from ..host2..)
 TARGET=0  (from target2:0:0 <--) specify new timeout as

 # ./ql-set-cmd-timeout.sh 2 0 60 

5.2 Command Line Options

[DEFAULT]
       Displays the timeout on devices connected to the all HOSTs.
       These devices must be QLogic adapters.

[HOST]
       Specifies the HOST value to see the timeout on devices 
       connected to the HOST. These devices must be QLogic adapters.

[HOST] [TARGET]	
       Specifies the HOST and TARGET values to see the timeout on 
       devices connected to the TARGET. These devices must be QLogic 
       adapters.

[HOST] [TARGET] [TIMEOUT] 
       Specifies the HOST, TARGET, and TIMEOUT values to set the 
       timeout on devices connected to the TARGET. These devices 
       must be QLogic adapters.

  -h,  --help, ?
       Displays the help text.

  -i,  --interactive
       Invokes the menu-driven program.

Examples:

 * To display the timeout of all devices connected to QLogic adapters,
   enter the following command:

    # ./ql-set-cmd-timeout.sh   
 
 * To display the timeout of all devices connected to HOST 2, enter 
   the following command:

    # ./ql-set-cmd-timeout.sh 2  

 * To display the timeout of devices connected on HOST 2, TARGET 0,  
   enter the following command: 

    # ./ql-set-cmd-timeout.sh 2 0  

 * To set a timeout of 30 seconds on HOST 2, TARGET 0, enter the 
   following command:

    # ./ql-set-cmd-timeout.sh 2 0 30 
		
 * To invoke the menu, enter one of the following commands:

    # ./ql-set-cmd-timeout.sh -i
    # ./ql-set-cmd-timeout.sh --interactive

 * To view help, enter one of the following commands:

    # ./ql-set-cmd-timeout.sh -h
    # ./ql-set-cmd-timeout.sh --help

5.3 Menu Options 

The utility provides a menu-driven interface that provides finer 
control of the operation.

To invoke the menu, use the -i or --interactive option with the
ql-set-cmd-timeout utility command:

 # ./ql-set-cmd-timeout.sh -i

Invoking this command opens the main menu.

MAIN MENU
   1. HOST2
   2. HOST3
   3. SELECT ALL HOSTS
   4. SET COMMON TIMEOUT FOR ALL HOSTS
   5. QUIT

   1: HOST2 

   This option appears if the system has a QLogic adapter installed 
   with a QLogic driver loaded. The host number differs from system 
   to system; as an example, the host numbers 2 and 3 are shown here.

   Selecting the HOST by specifying the index number opens the next 
   menu, where you can select a specific target.

   Select Target for HOST2
   1. Target 0
   2. Target 1
   3. Target 2
   4. Target 3
   5. GO TO PREVIOUS MENU
   6. Quit

   Please select one of the options above: 2

   Select the TARGET by specifying the index number to open the next
   menu, where you can select a specific action.
	
   Select option:
   1. MODIFY TIMEOUT
   2. DISPLAY TIMEOUT
   3. GO BACK TO PREVIOUS MENU
   4. QUIT

   Please select one of the options above: 1
   Please enter new timeout for selected device: 60

   Modifying the devices......

   DEVICE          OLD TIMEOUT             NEW TIMEOUT
   2:0:0:0             4                       60
   2:0:0:4             4                       60
   2:0:0:5             4                       60
   2:0:0:6             4                       60
   Hit any key to continue......
	
   Select the DISPLAY TIMEOUT option to display the following:   

   Select option:
   1. MODIFY TIMEOUT
   2. DISPLAY TIMEOUT
   3. GO BACK TO PREVIOUS MENU
   4. QUIT

   Please select one of the options above: 2

   DEVICE                         TIMEOUT
   2:0:0:0                          60
   2:0:0:4                          60
   2:0:0:5                          60
   2:0:0:6                          60
   Hit any key to continue......
		
   2. SELECT ALL HOSTS

   Select this option to select targets on all the HOSTS, and then 
   enter a timeout for each one.
	
   For example:

   MAIN MENU
   1. HOST2
   2. HOST3
   3. SELECT ALL HOSTS
   4. SET COMMON TIMEOUT FOR ALL HOSTS
   5. QUIT

   Please select one of the options above: 3

   Select the SELECT ALL HOSTS option by specifying the index number  
   to open the next menu, where you can select a specific action.

   Select option:
   1. MODIFY TIMEOUT
   2. DISPLAY TIMEOUT
   3. GO BACK TO PREVIOUS MENU
   4. QUIT

   Please select one of the options above: 1
   
   Enter new timeout for HOST 2 and TARGET 0: 40

   Modifying the devices......

   DEVICE          OLD TIMEOUT             NEW TIMEOUT
   2:0:0:0             50                      40
   2:0:0:4             50                      40
   Enter new timeout for HOST 2 and TARGET 1: 5
   
   Modifying the devices......

   DEVICE          OLD TIMEOUT             NEW TIMEOUT
   2:0:1:0             50                      5
   2:0:1:1             50                      5

   Select the DISPLAY TIMEOUT option to display the following.
		
   Select option:
   1. MODIFY TIMEOUT
   2. DISPLAY TIMEOUT
   3. GO BACK TO PREVIOUS MENU
   4. QUIT

   Please select one of the options above: 2	

   DEVICE          OLD TIMEOUT             NEW TIMEOUT
   2:0:0:0             50                      40
   2:0:0:4             50                      40

   DEVICE          OLD TIMEOUT             NEW TIMEOUT
   2:0:1:0             50                      5
   2:0:1:1             50                      5
   Hit any key to continue......

   3. SET COMMON TIMEOUT FOR ALL HOSTS

   Select this option to set the timeout on all the devices connected
   to all the targets on all QLogic adapters.

   4. QUIT

   Exits the ql-set-cmd-timeout utility.


6. Additional Notes 

None


7. Known Issues and Workarounds 

None


8. Contacting Support 

Please feel free to contact your QLogic approved reseller or QLogic 
Technical Support at any phase of integration for assistance. QLogic
Technical Support can be reached by the following methods: 

  Web:    http://support.qlogic.com
 E-mail: support@qlogic.com


(c) Copyright 2009. All rights reserved worldwide. QLogic, the QLogic 
logo, and the Powered by QLogic logo are registered trademarks of
QLogic Corporation. All other brand and product names are trademarks 
or registered trademarks of their respective owners. 
