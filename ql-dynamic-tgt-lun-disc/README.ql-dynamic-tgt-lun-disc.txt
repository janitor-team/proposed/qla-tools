﻿
             Dynamic TGT-LUN Discovery Utility for Linux
                                Readme

                         QLogic Corporation.
                         All rights reserved.
  

Table of Contents

1. Package Contents 
2. Requirements 
3. OS Support 
4. Supported Features 
5. Using the Dynamic TGT-LUN Discovery Utility   
   5.1 Starting the Utility 
   5.2 Command Line Options
   5.3 Menu Options 
6. Additional Notes 
7. Known Issues and Workarounds 
8. Contacting Support  


1. Package Contents 

The Dynamic TGT-LUN Discovery Utility package contains the following: 

 * COPYING - GNU General Public License that describes user rights to 
   copy, distribute, and use the open source content in this Linux 
   tool.

 * ql-dynamic-tgt-lun-disc.sh - Script file used to scan the QLogic 
   adapters for all the logical unit numbers (LUNs).

 * README.ql-dynamic-tgt-lun-disc.txt - This readme file.

 * revision.qldynamic.txt - Text file that identifies the changes 
   made between versions of this package. 

 * sg3_utils-1.23.tgz - Package containing utilities that send SCSI
   commands to the scsi_devices.


2. Requirements

The Dynamic TGT-LUN Discovery Utility (ql-dynamic-tgt-lun-disc.sh)
requires one of the Linux platforms listed in section 3, OS Support. 


3. OS Support

The Dynamic TGT-LUN Discovery Utility for Linux runs on the following
OS platforms:

 * Red Hat RHEL AS 3.0 (32-bit, 64-bit) on Intel x86, Intel EM64T, 
   AMD64

 * Red Hat RHEL AS 4.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64

 * Red Hat RHEL AS 5.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64

 * Red Hat RHEL AS 7.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
	x86_64
 
 * Novell SLES 8.0 (32-bit, 64-bit) on x86, AMD64

 * Novell SLES 9 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64 
   
 * Novell SLES 10 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64

NOTE: For specific OS service packs (SP) and updates, refer to the 
descriptions where this software version is posted on the QLogic Web
site:
  http://support.qlogic.com/support/drivers_software.aspx


4. Supported Features

The Dynamic TGT-LUN Discovery Utility provides the following features:

 * Re-scans all the QLogic adapters for new LUNs.
 
 * Re-scans and removes lost LUNs from the system.

 * By default, scans up to 256 LUNs and allows you to set the maximum
   number of LUNs to scan. 
 
 * By default, scans all QLogic adapters and allows you to select a 
   specific adapter to scan.
 
 * Displays information for each adapter.
 
 * Provides an option for proc-based scanning, as an alternative to 
   the default sysfs-based scanning.

 * Provides an option for extended scanning to rescan the devices 
   whose disk size has changed. 

 * Supports QLogic FC Driver Versions 7.xx.xx and 8.xx.xx. 

 * Also supports QLogic ISCSI Driver Versions 3.xx.xx.xx and 
   5.xx.xx.xx 


5. Using the Dynamic TGT-LUN Discovery Utility

This utility scans for newly-added LUNs. After adding new LUNs, you 
do not need to unload and then reload the QLogic FC driver, or reboot
the system. To see the newly-added LUNs, run the 
ql-dynamic-tgt-lun-disc.sh utility.

The following sections describe how to use this utility:

 * 5.1 Starting the Utility 
 * 5.2 Command Line Options
 * 5.3 Menu Options 

5.1 Starting the Utility 

To start this utility, run the following command: 

 # ./ql-dynamic-tgt-lun-disc.sh [ -is | --iscsi ]

By default, the utility re-scans the QLogic FC adapters for new LUNs.

5.2 Command Line Options
 
 -al, --allow-lip
    Allows the utility to issue the loop initialization processor 
    (LIP) whenever required. By default, the LIP is not issued, even 
    if it is required for scanning LUNs. If the utility does not 
    discover the necessity of LIP, this option is ignored.
 
 -cl, --current-luns
    Displays current LUNs on the system.		
	
 -e, --extended-scan
    Rescans the LUNs to identify any change in attributes of existing
    LUNs. This option can also be used in combination of scan/refresh
    or maximum LUNs.

 -h, --help, ?
    Prints the help text.

 -i, --interactive
    Invokes the menu-driven mode of the utility.

 -is, --iscsi
    Operates on ISCSI adapters. This option can be used in combination
    with any other supported option.

 -m, --max-lun
    Sets the maximum LUNs to be scanned.

 -p, --proc
    Uses the proc file system to perform LUN scanning on the 2.6 
    kernel. On a 2.4 kernel, the LUN scanning is based on proc file 
    system only.	

 -s, --scan [-r|--refresh]
    Re-scans all the devices connected to the QLogic adapter. To 
    remove LUNs that are lost, use the -r|--refresh option.
 
    CAUTION: Take care when using the Refresh option because it
    removes the existing LUNs before performing a re-scan.

The following examples each list two or three command options that you
can use to perform specific operations. Enter only one command.

 * To re-scan all the adapters:

     # ./ql-dynamic-tgt-lun-disc.sh 
     # ./ql-dynamic-tgt-lun-disc.sh -s 
     # ./ql-dynamic-tgt-lun-disc.sh --scan
		
 * To re-scan and remove any lost LUNs:

     # ./ql-dynamic-tgt-lun-disc.sh -s -r 
     # ./ql-dynamic-tgt-lun-disc.sh --scan --refresh

 * To run a proc-based scan of all the adapters:

     # ./ql-dynamic-tgt-lun-disc.sh -s -p
     # ./ql-dynamic-tgt-lun-disc.sh --scan --proc

 * To set the maximum LUNs to be scanned:

     # ./ql-dynamic-tgt-lun-disc.sh -m 124
     # ./ql-dynamic-tgt-lun-disc.sh -s -m 124

 * To identify any changes in attributes:
	
     # ./ql-dynamic-tgt-lun-disc.sh -e
     # ./ql-dynamic-tgt-lun-disc.sh --extended-scan

 * To use the "--extended-scan" option in combination with scan/
   refresh and max LUNs:
	
     # ./ql-dynamic-tgt-lun-disc.sh -e -s
     # ./ql-dynamic-tgt-lun-disc.sh -e -r
     # ./ql-dynamic-tgt-lun-disc.sh -e -s -m 120

 * To invoke the menu-driven utility:

     # ./ql-dynamic-tgt-lun-disc.sh -i
     # ./ql-dynamic-tgt-lun-disc.sh --interactive

 * To view the current LUNs on the system:

     # ./ql-dynamic-tgt-lun-disc.sh -cl
     # ./ql-dynamic-tgt-lun-disc.sh --current-luns

 * To view help:

     # ./ql-dynamic-tgt-lun-disc.sh -h
     # ./ql-dynamic-tgt-lun-disc.sh --help

5.3 Menu Options 

The utility provides a menu-driven interface that provides finer
control of the operation.

To invoke the menu, use the -i or --interactive option with the 
ql-dynamic-tgt-lun-disc utility. For example:

 # ./ql-dynamic-tgt-lun-disc.sh -i

The following sections describe the utility menus.

5.3.1 MAIN MENU

 1: ALL HOSTS SCAN
 2: ALL HOST SCAN & REFRESH
 3: ALL HOSTS EXTENDED SCAN 
 4: SELECT HOST TO SCAN
 5: SET MAX LUNs TO SCAN (Current: 256)
 6: DISPLAY CURRENT LUNS
 7: QUIT

 1: ALL HOSTS SCAN

  Scans all the QLogic adapters connected in the system. A message
  indicates the new LUN found.

 2: ALL HOST SCAN & REFRESH

  Scans all the QLogic adapters connected in the system and removes 
  LUNs that no longer exist.

  For example, if LUN 1 is seen on Host:2, Bus:0, and Device:0, a 
  corresponding entry exists in /proc/scsi/scsi. For example:
	
    Host: scsi2 Channel: 00 Id: 00 Lun: 01

  If the LUN is removed, the system still shows the LUN present in 
  /proc/scsi/scsi. To remove any lost LUNS, use this option to 
  re-scan the adapter.

  3: ALL HOSTS EXTENDED SCAN

  Scans all the devices connected to QLogic adapters in the 
  system. This rescans the devices whose disk size has changed.  

  4: SELECT HOST TO SCAN

  Invokes the menu to select a specific QLogic adapter to be 
  scanned. (See section 5.3.2.)

  5: SET MAX LUNs TO SCAN (Current: 256)

  Changes the maximum number of LUNs to be scanned. By default, the 
  utility scans a maximum of 256 LUNs.

  Note[*] : Here default value is set to LUN ID : 256, if LUN ID is greater
  than 256 and less than 4095, then it will scan the LUN as per need basis,
  but bydefault it will scan LUN upto 256

  6: DISPLAY CURRENT LUNS

  Shows the current LUNs attached to all QLogic hosts in the system.

  7: QUIT

  Exits the ql-dynamic-tgt-lun-disc utility.

5.3.2 SELECT HOST TO SCAN

 1. HOST: scsi2
 2. HOST: scsi3
 3. SET SCAN TYPE (Current : SCAN ONLY)
 4. GO BACK TO PREVIOUS SCREEN
 5. QUIT

  1. HOST: scsi<n>

  Specifies the adapter to be scanned. Select this option to start 
  re-scanning the adapter that corresponds to this host number.

  2. SET SCAN TYPE 

  Specifies whether re-scanning removes LUNs that no longer exist as a
  default. By default, the utility re-scans without removing LUNs from
  the system. Extended scan allows you to scan hosts whose disk size 
  has changed. This option allows you change to "HOST SCAN, REFRESH & 
  EXTENDED SCAN."

  3. GO BACK TO PREVIOUS SCREEN

  Returns to the main menu.
	
  4. QUIT

  Exits the ql-dynamic-tgt-lun-disc utility.


6. Additional Notes 

CAUTION: Take care when using the Refresh option because it removes
the existing LUNs before it re-scans.


7. Known Issues and Workarounds: 
	
None


8. Contacting Support 

Please feel free to contact your QLogic approved reseller or QLogic 
Technical Support at any phase of integration for assistance. QLogic
Technical Support can be reached by the following methods: 

 Web:    http://support.qlogic.com
 E-mail: support@qlogic.com


(c) Copyright 2009. All rights reserved worldwide. QLogic, the QLogic 
logo, and the Powered by QLogic logo are registered trademarks of
QLogic Corporation. All other brand and product names are trademarks 
or registered trademarks of their respective owners. 
