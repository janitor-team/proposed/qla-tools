﻿
              FC HBA Change LUN State Utility for Linux
                                Readme

                         QLogic Corporation.
                         All rights reserved. 


Table of Contents

1. Package Contents 
2. Requirements 
3. OS Support 
4. Supported Features 
5. Using the FC HBA Change LUN State Utility 
   5.1 Starting the Utility
   5.2 Command Line Options 
   5.3 Menu Options   
6. Application Notes 
7. Known Issues and Workarounds 
8. Contacting Support  


1. Package Contents 

The FC HBA Change LUN State Utility package contains the following: 

 * COPYING - GNU General Public License that describes user rights to
   copy, distribute, and use the open source content in this Linux 
   tool.

 * ql-lun-state-online.sh - Script file used to change the state of
   LUNs connected to QLogic adapters from offline to online/running.

 * README.ql-lun-state-online.txt - This readme file.

 * revision.qllunstateonline.txt - Text file that identifies the 
   changes made between versions of this package. 


2. Requirements

The FC HBA Change LUN State Utility requires one of the Linux 
platforms identified in section 3, OS Support. 


3. OS Support

The FC HBA Change LUN State Utility runs on the following OS 
platforms:

 * Red Hat RHEL AS 4.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64 platforms

 * Red Hat RHEL AS 5.0 (32-bit, 64-bit) on Intel IA64, Intel EM64T, 
   AMD64 platforms

 * Novell SLES 9 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64 
   platforms
 
 * Novell SLES 10 (32-bit, 64-bit) on Intel IA64, Intel EM64T, AMD64 
   platforms

NOTE: For specific OS service packs (SP) and updates, refer to the 
descriptions where this software version is posted on the QLogic Web
site:
http://support.qlogic.com/support/drivers_software.aspx


4. Supported Features

The FC HBA Change LUN State Utility (ql-lun-state-online.sh):

 * Allows you to select a specific host for enabling LUNs.
 
 * Provides an interactive menu, which lets you select a specific LUN.
 
 * Enables all offline LUNs with single command.
 
 * Lets you select specific targets for every host using a menu.

 * Supports Fibre Channel driver version 8.xx.xx.


5. Using the FC HBA Change LUN State Utility

This utility allows you to change the state of LUNs connected to a
QLogic adapter from offline to online/running. The SCSI mid-layer may 
change a device's state to offline when it does not receive a response
from the device. When these devices are offline, the SCSI mid-layer 
ignores them.

For example, if a SCSI command times out on a specific device and
fails to recover the device, the SCSI mid-layer marks the device as 
offline. Later, when the device is online or accessible, you can use 
the ql-lun-state.sh utility to change the state to running or online. 

5.1 Starting the Utility

To start this script, enter one of the following commands:

 # ./ql-lun-state-online.sh <HOST NUMBER/S>
 # ./ql-lun-state-online.sh --all

For example:

For /sys/class/scsi_device/2:0:1:4/  HOST number - > 2	

For /sys/class/scsi_device/3:0:2:6/  HOST number - > 3
	
COMMAND:

 # ./ql-lun-state-online.sh 2 3 

This changes the state of all the offline LUNs to running. 

5.2 Command Line Options

Usage: 

./ql-lun-state-online.sh [OPTIONS]

Options:

 HOST NUMBER/S
       Scans for all offline LUNs on the specified HOSTS and enables 
       them.

  -a,  --all
       Enables all disabled LUNs.

  -i,  --interactive
       Starts the menu-driven program.

  -h,  --help, ?
       Displays help text.

Examples:

 * To enable all LUNs on HOST 2 and 4, enter the following command:

    # ./ql-lun-state-online.sh 2 4

 * To enable all LUNs on all adapters, enter one of the following 
   commands:
				
    # ./ql-lun-state-online.sh -a
    # ./ql-lun-state-online.sh --all

 * To invoke the menu, enter one of the following commands:

    # ./ql-lun-state-online.sh -i
    # ./ql-lun-state-online.sh --interactive

 * To view help, enter one of the following commands:
    # ./ql-lun-state-online.sh -h
    # ./ql-lun-state-online.sh --help

5.3 Menu Options 

The utility provides a menu-driven interface that provides finer 
control of the operation.

To invoke the menu, use the -i or --interactive option with the
ql-lun-state utility. For example: 

 # ./ql-lun-state-online.sh -i

The following describes the main menu.

   MAIN MENU
   1. HOST: 2 TGT: 0 LUN: 4
   2. HOST: 3 TGT: 1 LUN: 4
   3. MAKE ALL ONLINE
   4. QUIT

   Please select any one option:
	
   "1. HOST: 2 TGT: 0 LUN: 4" and "2. HOST: 3 TGT: 1 LUN: 4" are 
   system-specific options. These only appear if the QLogic adapter 
   has targets with LUN state "offline." To enable the specified 
   LUN, select the appropriate option. 

   MAIN MENU
   1. HOST: 2 TGT: 0 LUN: 4
   2. HOST: 3 TGT: 1 LUN: 4
   3. MAKE ALL ONLINE
   4. QUIT
     
   Please select any one option: 2
   Modifying to running HOST: 3 TGT: 1 LUN: 4

   3. MAKE ALL ONLINE

   This option enables all offline LUNs.

   MAIN MENU
   1. HOST: 2 TGT: 0 LUN: 4
   2. HOST: 3 TGT: 1 LUN: 4
   3. MAKE ALL ONLINE
   4. QUIT

   Please select any one option: 3

   Found LUN 4 on HOST 2 in offline state
   Modifying to running

   Found LUN 4 on HOST 3 in offline state
   Modifying to running
 
   4. QUIT
   This option quits the utility. As an alternative, you can quit
   the utility by typing q or x.


6. Application Notes 

None


7. Known Issues and Workarounds 

None


8. Contacting Support 

Please feel free to contact your QLogic approved reseller or QLogic 
Technical Support at any phase of integration for assistance. QLogic
Technical Support can be reached by the following methods: 

 Web:    http://support.qlogic.com
 E-mail: support@qlogic.com


(c) Copyright 2009. All rights reserved worldwide. QLogic, the QLogic 
logo, and the Powered by QLogic logo are registered trademarks of
QLogic Corporation. All other brand and product names are trademarks 
or registered trademarks of their respective owners. 
